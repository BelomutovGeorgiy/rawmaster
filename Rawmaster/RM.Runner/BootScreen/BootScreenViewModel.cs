﻿using Ninject;
using RM.BusinessLogic.BusinessLogic.Auth;
using RM.Runner.MainMenu;
using RM.UI.Shared;

namespace RM.Runner.BootScreen
{
    internal class BootScreenViewModel : ScreenBase
    {
        private bool _isAuthentificationEnabled = true;
        private IAuthorisationService _authService;

        private string _username;
        private string _password;

        [Inject]
        public void Inject(IAuthorisationService authService)
        {
            _authService = authService;
        }

        public string Username
        {
            get { return _username; }

            set { Set(ref _username, value, nameof(Username)); }
        }

        public string Password
        {
            get { return _password; }
            set { Set(ref _password, value, nameof(Password)); }
        }

        public void SignInCommand()
        {
            var canEnter = !_isAuthentificationEnabled || _authService.AuthenticateUser(Username, Password);
            if (canEnter)
            {
                OpenWindow<MainWindowViewModel>();
                TryClose();
            }
            
        }
    }
}
