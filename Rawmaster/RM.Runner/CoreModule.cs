﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using Ninject.Modules;
using RM.BusinessLogic.BusinessLogic.Auth;
using RM.BusinessLogic.BusinessLogic.Core.DataAccess;
using RM.BusinessLogic.BusinessLogic.Core.Services;
using RM.BusinessLogic.Invoices.Services;
using RM.Runner.MainMenu.MenuContentControl;

namespace RM.Runner
{
    class CoreModule : NinjectModule
    {
        public override void Load()
        {
            Bind<WindowMenuItem>().ToSelf();
            Bind<IWindowManager>().To<WindowManager>();
            Bind<IAuthorisationService>().To<AuthorisationService>();
            Bind(typeof(IRepository<>)).To(typeof(Repository<>));
            Bind(typeof(IServiceBase<>)).To(typeof(ServiceBase<>));
            Bind<IInvoiceService>().To<InvoiceService>();
            Bind<IActsService>().To<ActsService>();
        }
    }
}
