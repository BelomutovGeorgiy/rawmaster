﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows;
using Caliburn.Micro;
using Ninject;
using RM.Runner.BootScreen;
using RM.Runner.MainMenu;
using RM.UI;
using RM.UI.RM.UI.DataManager.Banks;
using RM.UI.Shared;

namespace RM.Runner
{
    public class RawmasterBootstrapper : BootstrapperBase
    {
        private IKernel _kernel;

        public RawmasterBootstrapper()
        {
            Initialize();
        }
        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<BootScreenViewModel>();
        }

        protected override object GetInstance(Type service, string key)
        {
            return _kernel.Get(service);
        }

        protected override void Configure()
        {
            _kernel = new StandardKernel(new CoreModule());
        }

        protected override void OnExit(object sender, EventArgs e)
        {
            _kernel.Dispose();
            base.OnExit(sender, e);
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return _kernel.GetAll(service);
        }

        protected override void BuildUp(object instance)
        {
            _kernel.Inject(instance);
        }

        protected sealed override IEnumerable<Assembly> SelectAssemblies()
        {
            return new[]
            {
                typeof(RawmasterBootstrapper).Assembly,
                typeof(UiBootstrapper).Assembly,
                typeof(SharedBootstrapper).Assembly
            };
        }
    }
}
