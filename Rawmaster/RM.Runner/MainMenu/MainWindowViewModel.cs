﻿using System.Collections.ObjectModel;
using RM.Runner.MainMenu.MenuContentControl;
using RM.UI.ClientPicker;
using RM.UI.Invoices.Buckwheat.InvoicesList;
using RM.UI.RM.UI.DataManager.Banks;
using RM.UI.RM.UI.DataManager.ClientTypes;
using RM.UI.RM.UI.DataManager.Regions;
using RM.UI.Shared;

namespace RM.Runner.MainMenu
{
    class MainWindowViewModel : ScreenBase
    {
        private MenuViewModel _menu;

        public MenuViewModel Menu
        {
            get { return _menu; }
            set
            {
                _menu = value;
                Set(ref _menu, value);
            }
        }

        public MainWindowViewModel()
        {
            var menu = new MenuViewModel
            {
                MenuItems = new ObservableCollection<MenuItem>
                {
                    new SubMenuItem
                    {
                        Text = "Specialist",
                        MenuItems = new ObservableCollection<MenuItem>
                        {
                          new SubMenuItem
                          {
                              Text = "Invoices",
                              MenuItems = new ObservableCollection<MenuItem>
                              {
                                  new WindowMenuItem(new InvoicesListViewModel()){Text = "Buckwheat Exchange"}
                              }
                          },
                          new MenuItem { Text = "Logs", IsImplemented = false },
                          new MenuItem { Text = "Reports", IsImplemented = false },
                          new MenuItem { Text = "Unloading", IsImplemented = false },
                          new WindowMenuItem(new ClientPickerViewModel()) { Text = "Client Loads"},
                        }
                    },
                    new SubMenuItem
                    {
                        Text = "Administration",
                        MenuItems = new ObservableCollection<MenuItem>
                        {
                            new MenuItem { Text = "Main Menu", IsImplemented = false },
                            new MenuItem { Text = "Database", IsImplemented = false },
                            new MenuItem { Text = "Configuration", IsImplemented = false },
                            new MenuItem { Text = "Compress system tables", IsImplemented = false },
                            new MenuItem { Text = "Utilities", IsImplemented = false },
                        }
                    },
                    new SubMenuItem
                    {
                        Text = "About",
                        MenuItems = new ObservableCollection<MenuItem>
                        {
                            new MenuItem { Text = "About Programm", IsImplemented = false },
                            new MenuItem { Text = "Configuraiton help", IsImplemented = false },
                            new MenuItem { Text = "Dialogs", IsImplemented = false },
                            new MenuItem { Text = "Table search", IsImplemented = false },
                            new MenuItem { Text = "About AWP", IsImplemented = false },
                            new MenuItem { Text = "Credits", IsImplemented = false },
                        }
                    },
                    new SubMenuItem
                    {
                        Text = "Data Manager",
                        MenuItems = new ObservableCollection<MenuItem>
                        {
                            new WindowMenuItem(new BanksEditorViewModel()){ Text = "Banks", IsImplemented = false},
                            new WindowMenuItem(new RegionsEditorViewModel()){ Text = "Regions", IsImplemented = false},
                            new WindowMenuItem(new ClientTypesViewModel()){ Text = "Client Types", IsImplemented = false},
                            new MenuItem { Text = "Districts", IsImplemented = false},
                            new MenuItem { Text = "Operations", IsImplemented = false},
                            new WindowMenuItem(new ClientPickerViewModel()) { Text = "Cliens"},
                        }
                    }
                }
            };

            Menu = menu;
        }
    }
}
