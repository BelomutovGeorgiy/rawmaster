﻿using Caliburn.Micro;
using Ninject;
using RM.UI.Shared;
using Action = System.Action;

namespace RM.Runner.MainMenu.MenuContentControl
{
    public class WindowMenuItem : MenuItem
    {
        private IWindowManager _windowManager;

        public WindowMenuItem()
        {
        }

        public Action Action
        {
            get { return ItemAction; }
            set { ItemAction = value; }
        }

        [Inject]
        public void Inject(IWindowManager windowManager)
        {
            _windowManager = windowManager;
        }

        public ScreenBase ViewModel { get; set; }

        public WindowMenuItem(ScreenBase viewModel)
        {
            IoC.BuildUp(this);
            ViewModel = viewModel;
            Action = () =>
            {

                IoC.BuildUp(ViewModel);
                _windowManager.ShowDialog(ViewModel);
            };
        }
    }
}
