﻿using System.Collections.ObjectModel;
using Caliburn.Micro;

namespace RM.Runner.MainMenu.MenuContentControl
{
    class MenuViewModel : Screen
    {
        private ObservableCollection<MenuItem> _menuItems;
        public ObservableCollection<MenuItem> MenuItems
        {
            get
            {
                return _menuItems;
            }
            set
            {
                _menuItems = value;
                NotifyOfPropertyChange(() => MenuItems);
            }
        }
    }
}