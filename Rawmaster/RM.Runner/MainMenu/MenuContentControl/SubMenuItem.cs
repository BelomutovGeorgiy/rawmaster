﻿using System.Collections.ObjectModel;

namespace RM.Runner.MainMenu.MenuContentControl
{
    public class SubMenuItem : MenuItem
    {
        public ObservableCollection<MenuItem> MenuItems { get; set; }
    }
}
