﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Action = System.Action;

namespace RM.Runner.MainMenu.MenuContentControl
{
    public class MenuItem : ObservableCollection<MenuItem>
    {
        protected Action ItemAction;

        public Dictionary<string, object> MenuParameter { get; set; }

        public int Id { get; set; }

        public string Tag { get; set; }

        public int Position { get; set; }

        public string Text { get; set; }

        public string GroupName { get; set; }

        public bool StaysOpenOnClick { get; set; }

        public bool IsEnabled { get; set; }

        public bool IsVisible { get; set; }

        public bool IsImplemented { get; set; } = true;
        public void SelectItemCommand()
        {
            ItemAction?.Invoke();
        }
    }
}
