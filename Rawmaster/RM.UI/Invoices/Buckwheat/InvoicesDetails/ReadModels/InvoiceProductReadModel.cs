﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using RM.UI.Annotations;

namespace RM.UI.Invoices.Buckwheat.InvoicesDetails.ReadModels
{
    public class InvoiceProductReadModel : INotifyPropertyChanged
    {
        private decimal _pricePerTonn;
        private int _vat;
        private int _paymentPercent;

        public int Id { get; set; }

        public string Name { get; set; }

        public int BagWeight { get; set; }

        public int Amount { get; set; }

        public int Output => Amount - PaymentAmount;

        public double Recalculate { get; set; }

        public decimal PricePerTonn
        {
            get { return _pricePerTonn; }
            set
            {
                if (_pricePerTonn != value)
                {
                    _pricePerTonn = value;
                    OnPropertyChanged(nameof(PricePerTonn));
                }
            }
        }

        public int NaturalPaymentPercent
        {
            get { return _paymentPercent; }
            set
            {
                if (_paymentPercent != value)
                {
                    _paymentPercent = value;
                    OnPropertyChanged(nameof(NaturalPaymentPercent));
                }
            }
        }

        public int Vat
        {
            get { return _vat; }
            set
            {
                if (_vat != value)
                {
                    _vat = value;
                    OnPropertyChanged(nameof(Vat));
                }
            }
        }

        public decimal PaymentPrice => PricePerTonn * PaymentAmount; 

        public int PaymentAmount { get; private set; }

        public void CalculatePayment(decimal requiredPrice)
        {
            var cleanPrice = (requiredPrice * NaturalPaymentPercent / 100);
            var totalPrice = cleanPrice + cleanPrice * (Vat / 100);

            var requiredAmount = PricePerTonn != 0 ? totalPrice / PricePerTonn : 0;

            PaymentAmount = (int)Math.Round(requiredAmount);
            OnPropertyChanged(nameof(PaymentAmount));
            OnPropertyChanged(nameof(PaymentPrice));
            OnPropertyChanged(nameof(Output));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
