﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using RM.UI.Annotations;

namespace RM.UI.Invoices.Buckwheat.InvoicesDetails.ReadModels
{
    public class InvoiceGeneralDetails : INotifyPropertyChanged
    {
        public int TotalCorn { get; set; }

        public int PricePerTonn { get; set; }

        public int ProcessCost => TotalCorn / 1000 * PricePerTonn;

        public int Vat { get; set; }

        public int VatCost => Vat / 100 * ProcessCost;

        public int TotalCost => ProcessCost * VatCost;

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
