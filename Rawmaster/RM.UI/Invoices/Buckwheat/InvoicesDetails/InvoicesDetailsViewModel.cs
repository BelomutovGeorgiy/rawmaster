﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Ninject;
using RM.BusinessLogic.Invoices.Entities;
using RM.BusinessLogic.Invoices.Services;
using RM.UI.Invoices.Buckwheat.InvoicesDetails.ReadModels;
using RM.UI.Shared;

namespace RM.UI.Invoices.Buckwheat.InvoicesDetails
{
    public class InvoicesDetailsViewModel : ScreenBase
    {
        private IInvoiceService _invoiceService;

        private int _tortalCorn;
        private ObservableCollection<InvoiceProductReadModel> _products;
        private Invoice _invoice;

        public string ClientName => _invoice.Client.Name;

        [Inject]
        public void Inject(IInvoiceService invoiceService)
        {
            _invoiceService = invoiceService;
        }

        public Invoice Invoice
        {
            get { return _invoice; }
            set { Set(ref _invoice, value); }
        }


        public int TotalCorn
        {
            get { return _tortalCorn; }
            set
            {
                Set(ref _tortalCorn, value); 
                NotifyFields();
            }
        }

        public ObservableCollection<InvoiceProductReadModel> Products
        {
            get { return _products; }
            set { Set(ref _products, value); }
        }

        public decimal ProcessCost => (TotalCorn / 1000m) * Invoice.PricePerTonn;

        public decimal VatCost => (Invoice.Vat / 100m) * ProcessCost;

        public decimal TotalCost => ProcessCost + VatCost;

        public decimal PaymentRemains => TotalCost - (Products?.Sum(x => x.PaymentPrice) ?? 0);

        public InvoicesDetailsViewModel(Invoice invoice)
        {
            Invoice = invoice;
        }

        protected override void OnInitialize()
        {
            TotalCorn = _invoiceService.GetActsByInvoiceId(Invoice.Id).Sum(x => x.TotalWeight);
            FillProducts();
            base.OnInitialize();
            Products.CollectionChanged += (sender, args) => NotifyOfPropertyChange(nameof(Products));
            NotifyFields();

        }

        private void NotifyFields()
        {
            NotifyOfPropertyChange(nameof(ProcessCost));
            NotifyOfPropertyChange(nameof(VatCost));
            NotifyOfPropertyChange(nameof(TotalCost));
            NotifyOfPropertyChange(nameof(PaymentRemains));
        }

        public void RefreshCommand()
        {
            foreach (var p in Products)
            {
                p.CalculatePayment(TotalCost);
            }
            NotifyOfPropertyChange(nameof(Products));
            NotifyFields();
        }

        public void SaveCommand()
        {
            _invoiceService.UpdateInvoice(Invoice);

            var products = Invoice.Products;

            foreach (var p in products)
            {
                p.Vat = Products.FirstOrDefault(y => y.Id == p.Id)?.Vat ?? 0;
                p.PricePerTonn = Products.FirstOrDefault(y => y.Id == p.Id)?.PricePerTonn ?? 0;
                p.NaturalPaymentValue = Products.FirstOrDefault(y => y.Id == p.Id)?.NaturalPaymentPercent ?? 0;
            }
            _invoiceService.UpdateProducTransactionsForInvoice(products);
        }

        private void FillProducts()
        {
            var products = Invoice.Products.Select(x =>
                new InvoiceProductReadModel
                {
                    Id = x.Id,
                    BagWeight = 50,
                    Name = x.Product.Name,
                    Amount = Invoice.Acts
                        .SelectMany(y => y.Products)
                        .Where(y => y.Product.Id == x.Product.Id)
                        .Sum(y => y.Amount),
                    NaturalPaymentPercent = x.NaturalPaymentValue,
                    PricePerTonn = x.PricePerTonn
                }).ToList();
            foreach (var p in products)
            {
                p.CalculatePayment(TotalCost);
            }
            Products = new ObservableCollection<InvoiceProductReadModel>(products);
        }
    }
}
