﻿using System;
using System.Collections.Generic;
using System.Linq;
using Caliburn.Micro;
using Ninject;
using RM.BusinessLogic.BusinessLogic.Core.Services;
using RM.BusinessLogic.ClientManager.Entities;
using RM.BusinessLogic.Invoices;
using RM.BusinessLogic.Invoices.Entities;
using RM.BusinessLogic.Invoices.Services;
using RM.UI.ClientPicker;
using RM.UI.Shared;
using RM.UI.Shared.Utils.SearchComponent;

namespace RM.UI.Invoices.Buckwheat.AddInvoice
{
    public class InvoiceCreationViewModel : ScreenBase
    {
        private IServiceBase<Client> _clientService;
        private IServiceBase<Invoice> _invoiceService;
        private IInvoiceService _advancdInvoiceService;

        private int _invoiceNumber;
        private decimal _pricePerTonn;
        private int _vatAmount;
        private Client _selectedClient;
        private OperationType _selectedOperation;

        public List<OperationType> Operations { get; } = Enum.GetValues(typeof(OperationType)).Cast<OperationType>().ToList();

        public OperationType SelectedOperation
        {
            get { return _selectedOperation; }
            set { Set(ref _selectedOperation, value); }
        }

        public Client Client
        {
            get { return _selectedClient; }
            set { Set(ref _selectedClient, value); }
        }

        public int InvoiceNumber
        {
            get { return _invoiceNumber; }
            set { Set(ref _invoiceNumber, value); }
        }

        public decimal PricePerTonn
        {
            get { return _pricePerTonn; }
            set { Set(ref _pricePerTonn, value); }
        }

        public int VatAmount
        {
            get { return _vatAmount; }
            set { Set(ref _vatAmount, value); }
        }

        [Inject]
        public void Inject(IServiceBase<Client> clientService,
            IServiceBase<Invoice> invocieService,
            IInvoiceService advancdInvoiceService)
        {
            _clientService = clientService;
            _invoiceService = invocieService;
            _advancdInvoiceService = advancdInvoiceService;
        }

        public void SelectClientCommand()
        {
            var clientPicker = new ClientPickerViewModel();
            IoC.BuildUp(clientPicker);
            if (WindowManager.ShowDialog(clientPicker) == true)
            {
                Client = clientPicker.SelectedItem;
            }
        }

        public void SaveCommand()
        {
            var invoice = new Invoice
            {
                Client = _selectedClient,
                IsArchieved = false,
                Aproximate = 0,
                ClientId = _selectedClient.Id,
                Archived = 0,
                CreationDate = DateTime.Now,
                IsCompleted = false,
                // TODO: Add managing
                RawId = 12,
                OperationType = SelectedOperation,
                Number = InvoiceNumber,
                PricePerTonn = PricePerTonn,
                Vat = VatAmount
            };

            var id = _invoiceService.Add(invoice);
            _advancdInvoiceService.CreaateProducTransactionsForInvoice(id);
            TryClose(true);
        }
    }
}
