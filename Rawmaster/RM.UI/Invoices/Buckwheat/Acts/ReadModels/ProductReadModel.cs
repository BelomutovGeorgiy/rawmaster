﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using RM.UI.Annotations;

namespace RM.UI.Invoices.Buckwheat.Acts.ReadModels
{
    public class ProductReadModel : INotifyPropertyChanged
    {
        private int _amount;
        private string _name;

        internal int ProductId { get; set; }

        public string Name
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    _name = value;
                    OnPropertyChanged(nameof(Name));
                }
            }
        }

        public int Amount
        {
            get { return _amount; }
            set
            {
                if (_amount != value)
                {
                    _amount = value;
                    OnPropertyChanged(nameof(Amount));
                }
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
