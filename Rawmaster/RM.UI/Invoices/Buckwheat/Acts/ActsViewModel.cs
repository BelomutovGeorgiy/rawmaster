﻿using System.Collections.ObjectModel;
using System.Linq;
using Ninject;
using RM.BusinessLogic.Invoices.Entities;
using RM.BusinessLogic.Invoices.Services;
using RM.UI.Invoices.Buckwheat.Acts.ReadModels;
using RM.UI.Shared.MasterDetail;

namespace RM.UI.Invoices.Buckwheat.Acts
{
    public class ActsViewModel : MasterDetailBaseViewModel<Act>
    {
        private IInvoiceService _invoiceService;
        private IActsService _actsService;

        private Invoice _invoice;

        public ObservableCollection<ProductReadModel> Products { get; set; }

        public string ClientName => _invoice.Client.Name;

        public Invoice Invoice
        {
            get { return _invoice; }
            set { Set(ref _invoice, value); }
        }

        [Inject]
        public void Inject(IInvoiceService invoiceService,
            IActsService actsService)
        {
            _invoiceService = invoiceService;
            _actsService = actsService;
        }

        public ActsViewModel(Invoice invoice)
        {
            _invoice = invoice;
        }

        protected override void RefreshItems()
        {
            Items = new ObservableCollection<Act>(Invoice.Acts);
            NotifyOfPropertyChange(nameof(Items));
        }

        protected override void OnInitialize()
        {
            base.OnInitialize();
            var productList = _invoiceService.GetAvailableProducts(Invoice.Id)
                .Select(x => new ProductReadModel
                {
                    ProductId = x.Id,
                    Name = x.Name,
                    Amount = 0
                }).ToList();
            Products = new ObservableCollection<ProductReadModel>(productList);
            NotifyOfPropertyChange(nameof(Products));
            Products.CollectionChanged += (sender, args) => NotifyOfPropertyChange(nameof(Products));

            RefreshItems();

        }

        protected override void Save()
        {
            SelectedItem.InvoiceId = Invoice.Id;
            base.Save();

            var products = Products.Select(x => new ActProduct
            {
                ActId = SelectedItem.Id,
                Amount = x.Amount,
                ProductId = x.ProductId
            }).ToList();

            if (IsNewItem)
            {
                _actsService.AddProductTransactions(products);
            }
            else
            {
                _actsService.UpdateProductTransactions(products);
            }

        }

        protected override void OnSelectionChanged()
        {
            var actProducts = SelectedItem?.Products;
            if (Products != null && actProducts != null)
            {
                foreach (var product in Products)
                {
                    product.Amount = actProducts.FirstOrDefault(x => x.ProductId == product.ProductId)?.Amount ?? 0;

                }
            }
            base.OnSelectionChanged();
        }
    }
}
