﻿using System.Collections.ObjectModel;
using Ninject;
using RM.BusinessLogic.Invoices.Entities;
using RM.BusinessLogic.Invoices.Services;
using RM.UI.Invoices.Buckwheat.Acts;
using RM.UI.Invoices.Buckwheat.AddInvoice;
using RM.UI.Invoices.Buckwheat.InvoicesDetails;
using RM.UI.Shared.MasterDetail;

namespace RM.UI.Invoices.Buckwheat.InvoicesList
{
    public class InvoicesListViewModel : MasterDetailBaseViewModel<Invoice>
    {
        private IInvoiceService _invoiceService;

        [Inject]
        public void Inject(IInvoiceService invoiceService)
        {
            _invoiceService = invoiceService;
        }

        protected override void RefreshItems()
        {
            Items = new ObservableCollection<Invoice>(_invoiceService.GetAll(true, showArchieved));
            NotifyOfPropertyChange(nameof(Items));
        }

        public bool CanDetailsCommand => SelectedItem != null;

        public void DetailsCommand()
        {
            var fullInvoice = _invoiceService.GetWithFullLoadById(SelectedItem.Id);
            OpenWindow<InvoicesDetailsViewModel>(fullInvoice);
        }

        public bool CanActsCommand => SelectedItem != null;

        public void ActsCommand()
        {
            var fullInvoice = _invoiceService.GetWithFullLoadById(SelectedItem.Id);
            OpenWindow<ActsViewModel>(fullInvoice);
        }

        public void AddNewCommand()
        {
            OpenWindow<InvoiceCreationViewModel>();
        }

        protected override void NotifyFields()
        {
            base.NotifyFields();
            NotifyOfPropertyChange(nameof(CanActsCommand));
            NotifyOfPropertyChange(nameof(CanDetailsCommand));
        }
    }
}
