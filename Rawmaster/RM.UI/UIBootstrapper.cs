﻿using System;
using System.Collections.Generic;
using Caliburn.Micro;
using Ninject;

namespace RM.UI
{
    public class UiBootstrapper : BootstrapperBase
    {
        private IKernel _kernel;

        public UiBootstrapper()
        {
            Initialize();
        }


        protected override object GetInstance(Type service, string key)
        {
            return _kernel.Get(service);
        }

        protected override void Configure()
        {
            _kernel = new StandardKernel();
        }

        protected override void OnExit(object sender, EventArgs e)
        {
            _kernel.Dispose();
            base.OnExit(sender, e);
        }

        protected override void BuildUp(object instance)
        {
            _kernel.Inject(instance);
        }
    }
}
