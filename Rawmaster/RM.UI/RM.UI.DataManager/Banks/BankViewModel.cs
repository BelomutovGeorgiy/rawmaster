﻿using RM.BusinessLogic.DataManager.Entities;
using RM.UI.Shared.MasterDetail;

namespace RM.UI.RM.UI.DataManager.Banks
{
    class BankViewModel : ItemViewModel<Bank>
    {

        public override string ItemName => Model?.Name + (Model?.IsArchieved == true ? " (Archived)" : string.Empty) ?? "New Item";

        public BankViewModel(Bank model) : base(model)
        {
        }
    }
}
