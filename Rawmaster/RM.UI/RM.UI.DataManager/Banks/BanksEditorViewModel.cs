﻿using System.Linq;
using RM.BusinessLogic.DataManager.Entities;
using RM.UI.Shared.MasterDetail;

namespace RM.UI.RM.UI.DataManager.Banks
{
    public class BanksEditorViewModel : MasterDetailBaseViewModel<Bank>
    {
        protected override void OnInitialize()
        {
            DetailsViewModel = new BankViewModel(new Bank());
            base.OnInitialize();
        }
    }
}
