﻿using System.Linq;
using RM.BusinessLogic.DataManager.Entities;
using RM.UI.Shared.MasterDetail;

namespace RM.UI.RM.UI.DataManager.Regions
{
    public class RegionsEditorViewModel : MasterDetailBaseViewModel<Region>
    {
        protected override void OnInitialize()
        {
            DetailsViewModel = new RegionViewModel(new Region());
            base.OnInitialize();
        }
    }
}
