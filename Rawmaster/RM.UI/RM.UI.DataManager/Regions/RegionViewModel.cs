﻿using RM.BusinessLogic.DataManager.Entities;
using RM.UI.Shared.MasterDetail;

namespace RM.UI.RM.UI.DataManager.Regions
{
    class RegionViewModel : ItemViewModel<Region>
    {

        public override string ItemName => Model?.Name + (Model?.IsArchieved == true ? " (Archived)" : string.Empty) ?? "New Item";

        public RegionViewModel(Region model) : base(model)
        {
        }
    }
}
