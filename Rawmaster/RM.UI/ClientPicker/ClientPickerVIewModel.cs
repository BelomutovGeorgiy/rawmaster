﻿using System.Collections.Generic;
using System.Linq;
using Ninject;
using RM.BusinessLogic.BusinessLogic.Core.Services;
using RM.BusinessLogic.ClientManager.Entities;
using RM.UI.Shared.MasterDetail;

namespace RM.UI.ClientPicker
{
    public class ClientPickerViewModel : MasterDetailBaseViewModel<Client>
    {
        private IServiceBase<ClientType> _clientTypesService;

        private ClientType _clientType;

        [Inject]
        public void Inject(IServiceBase<ClientType> clientTypesService)
        {
            _clientTypesService = clientTypesService;
        }

        protected override void OnInitialize()
        {
            Types = _clientTypesService.GetAll(false);
            SelectedType = Types.FirstOrDefault();
            NotifyOfPropertyChange(nameof(Types));
            base.OnInitialize();
        }

        public List<ClientType> Types { get; set; }

        public ClientType SelectedType
        {
            get { return _clientType; }
            set { Set(ref _clientType, value); }
        }

        protected override void Save()
        {
            SelectedItem.CategoryId = SelectedType.Id;
            base.Save();
        }

        protected override void OnSelectionChanged()
        {
            if (SelectedItem != null)
            {
                SelectedType = Types.FirstOrDefault(x => x.Id == SelectedItem.CategoryId);
            }

            base.OnSelectionChanged();
        }
    }
}
