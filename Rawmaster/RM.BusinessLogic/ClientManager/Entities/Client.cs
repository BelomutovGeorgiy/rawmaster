﻿using LinqToDB.Mapping;
using RM.BusinessLogic.BusinessLogic.Core.DataAccess;

namespace RM.BusinessLogic.ClientManager.Entities
{
    [Table("Clients")]
    public class Client : EntityBase
    {
        [Column]
        public string Name { get; set; }

        [Column]
        public string Address { get; set; }

        [Column]
        public string ContactName { get; set; }
        
        [Column("CatrgoryId")]
        public int CategoryId { get; set; }
    }
}
