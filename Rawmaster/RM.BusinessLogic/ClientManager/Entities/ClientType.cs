﻿using LinqToDB.Mapping;
using RM.BusinessLogic.BusinessLogic.Core.DataAccess;

namespace RM.BusinessLogic.ClientManager.Entities
{
    [Table("ClientCategories")]
    public class ClientType : EntityBase
    {
        [Column]
        public string Name { get; set; }
    }
}
