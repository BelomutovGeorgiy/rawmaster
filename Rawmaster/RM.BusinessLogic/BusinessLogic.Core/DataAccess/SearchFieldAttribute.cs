﻿using System;

namespace RM.BusinessLogic.BusinessLogic.Core.DataAccess
{
    [AttributeUsage(AttributeTargets.Property)]
    public class SearchFieldAttribute : Attribute
    {
        public string DisplayName { get; }

        public SearchFieldAttribute(string displayName)
        {
            DisplayName = displayName;
        }
    }
}
