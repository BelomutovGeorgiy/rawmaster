﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using LinqToDB;
using LinqToDB.Data;
using LinqToDB.DataProvider.SqlServer;

namespace RM.BusinessLogic.BusinessLogic.Core.DataAccess
{
    public class Repository<T> : IRepository<T> where T : class, IEntity
    {
        private DataConnection _connection;

        public Repository()
        {
            _connection = SqlServerTools.CreateDataConnection(ConfigurationManager.ConnectionStrings["RawmasterDB"].ConnectionString);
            LinqToDB.Common.Configuration.Linq.AllowMultipleQuery = true;
        }

        public int Add(T value)
        {
            return Convert.ToInt32(_connection.InsertWithIdentity(value));
        }

        public void AddRange(List<T> values)
        {
            foreach (var val in values)
            {
                _connection.InsertWithIdentity(val);
            }
        }

        public IQueryable<T> GetAll()
        {
            return _connection.GetTable<T>();
        }

        public T GetById(int id)
        {
            return _connection.GetTable<T>().FirstOrDefault(x => x.Id == id);
        }

        public void Update(int id, T value)
        {
            value.Id = id;
            _connection.Update(value);
            _connection.CommitTransaction();
        }
    }
}
