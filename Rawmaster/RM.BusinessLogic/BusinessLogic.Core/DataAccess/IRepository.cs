﻿using System.Collections.Generic;
using System.Linq;

namespace RM.BusinessLogic.BusinessLogic.Core.DataAccess
{
    public interface IRepository<T> where T : IEntity
    {
        IQueryable<T> GetAll();

        T GetById(int id);

        void Update(int id, T value);

        /// <summary>
        /// Inserts new instance to the DB
        /// </summary>
        /// <param name="value">instance to insert</param>
        /// <returns>Identity of the inserted entity</returns>
        int Add(T value);

        void AddRange(List<T> values);
    }
}
