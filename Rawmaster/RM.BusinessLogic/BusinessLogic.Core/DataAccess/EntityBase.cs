﻿using LinqToDB.Mapping;

namespace RM.BusinessLogic.BusinessLogic.Core.DataAccess
{
    public class EntityBase : IEntity
    {
        [SearchField("Id")]
        [Column("Id", IsIdentity = true)]
        [PrimaryKey]
        public int Id { get; set; }

        [NotColumn]
        public bool IsArchieved
        {
            get { return Archived == 1; }
            set { Archived = (byte) (value ? 1 : 0); }
        }

        [Column("Archived")]
        public byte Archived { get; set; }
    }
}
