﻿namespace RM.BusinessLogic.BusinessLogic.Core.DataAccess
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}
