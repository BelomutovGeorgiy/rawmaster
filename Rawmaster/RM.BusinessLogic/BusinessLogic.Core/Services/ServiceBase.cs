﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using RM.BusinessLogic.BusinessLogic.Core.DataAccess;
using RM.BusinessLogic.DataManager.Entities;

namespace RM.BusinessLogic.BusinessLogic.Core.Services
{
    public interface IServiceBase<T> where T : EntityBase
    {
        /// <summary>
        /// Added new item to the DB.
        /// </summary>
        /// <param name="item">An entity to insert</param>
        /// <returns>Identity of theinserted entity.</returns>
        int Add(T item);

        void Update(T item);

        T GetById(int id);

        List<T> GetAll(bool showArchieved);

        List<T> GetByPredicate(Expression<Func<T, bool>> predicate, bool showArchived = false);
    }

    public class ServiceBase<T> : IServiceBase<T> where T : EntityBase
    {
        protected IRepository<T> Repository;

        public ServiceBase(IRepository<T> repository)
        {
            Repository = repository;
        }

        public int Add(T item)
        {
            return Repository.Add(item);
        }

        public void Update(T item)
        {
            Repository.Update(item.Id, item);
        }

        public T GetById(int id) => Repository.GetById(id);

        public List<T> GetByPredicate(Expression<Func<T, bool>> predicate, bool showArchived = false)
        {
            return Repository.GetAll().Where(x => showArchived || x.Archived == 0).Where(predicate).ToList();
        }

        public List<T> GetAll(bool showArchieved)
        {
            var items = Repository.GetAll();
            return items.Where(x => showArchieved || x.Archived == 0).ToList();
        }
    }
}
