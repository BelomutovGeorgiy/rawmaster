﻿using LinqToDB.Mapping;
using RM.BusinessLogic.BusinessLogic.Core.DataAccess;

namespace RM.BusinessLogic.DataManager.Entities
{
    [Table("Banks")]
    public class Bank : EntityBase
    {
        [Column("RegionId")]
        public int RegionId { get; set; }

        [Column("Code", Length = 25)]
        [SearchField("Bank Code")]
        public string Code { get; set; }

        [SearchField("Bank Name")]
        [Column("Name", Length = 50)]
        public string Name { get; set; }

        [Column("Address")]
        public string Address { get; set; }

        [Column("Mfo")]
        public long Mfo { get; set; }
    }
}
