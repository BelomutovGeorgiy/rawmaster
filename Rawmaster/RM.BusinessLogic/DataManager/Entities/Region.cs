﻿using LinqToDB.Mapping;
using RM.BusinessLogic.BusinessLogic.Core.DataAccess;

namespace RM.BusinessLogic.DataManager.Entities
{
    [Table("Regions")]
    public class Region : EntityBase
    {
        [Column]
        public string Name { get; set; }

        [Column]
        public int DistrictId { get; set; }
    }
}
