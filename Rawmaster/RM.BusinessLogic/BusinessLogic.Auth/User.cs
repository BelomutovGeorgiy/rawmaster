﻿using LinqToDB.Mapping;
using RM.BusinessLogic.BusinessLogic.Core.DataAccess;

namespace RM.BusinessLogic.BusinessLogic.Auth
{
    [Table("Operators")]
    public class User : IEntity
    {
        [PrimaryKey]
        public int Id { get; set; }

        [Column(Name = "Username")]
        [NotNull]
        public string UserName { get; set; }

        [Column(Name = "Password")]
        [NotNull]
        public string Password { get; set; }

        [Column(Name = "DisplayName")]
        [Nullable]
        public string DisplayName { get; set; }
    }
}
