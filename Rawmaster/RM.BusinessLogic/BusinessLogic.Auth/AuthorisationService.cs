﻿using System.Linq;
using RM.BusinessLogic.BusinessLogic.Core.DataAccess;

namespace RM.BusinessLogic.BusinessLogic.Auth
{
    public interface IAuthorisationService
    {
        /// <summary>
        /// Authenticate user by its credentials
        /// </summary>
        /// <param name="username">username to authenticate</param>
        /// <param name="password">password to authenticate</param>
        /// <returns>Can Enter?</returns>
        bool AuthenticateUser(string username, string password);
    }
    public class AuthorisationService : IAuthorisationService
    {
        private IRepository<User> _repository;

        public AuthorisationService(IRepository<User> repository)
        {
            _repository = repository;
        }
        public bool AuthenticateUser(string username, string password)
        {
            var r = _repository.GetById(1);
            var result = _repository.GetAll().Where(u => u.UserName == username &&
                                            u.Password == password);
            if (result.FirstOrDefault() == null)
            {
                return false;
            }

            return true;
        }
    }
}
