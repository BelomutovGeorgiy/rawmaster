﻿using System;
using System.Collections.Generic;
using LinqToDB;
using LinqToDB.Mapping;
using RM.BusinessLogic.BusinessLogic.Core.DataAccess;

namespace RM.BusinessLogic.Invoices.Entities
{
    [Table("Acts")]
    public class Act : EntityBase
    {
        [Column]
        public int InvoiceId { get; set; }

        [Column("TTN_List")]
        public string TtnList { get; set; }

        [Column]
        public string CarNumber { get; set; }

        [Column("Date", DataType = DataType.Date)]
        public DateTime CreationDate { get; set; }

        [Column("Act_Number")]
        public int ActNumber { get; set; }

        [Column]
        public int TotalWeight { get; set; }

        [Association(ThisKey = nameof(Id), OtherKey = nameof(ActProduct.ActId))]
        public List<ActProduct> Products { get; set; }
    }
}
