﻿using LinqToDB.Mapping;
using RM.BusinessLogic.BusinessLogic.Core.DataAccess;

namespace RM.BusinessLogic.Invoices.Entities
{
    [Table("ProductActTransaction")]
    public class ActProduct : IEntity
    {
        [SearchField("Id")]
        [Column("Id", IsIdentity = true)]
        [PrimaryKey]
        public int Id { get; set; }

        [Column]
        public int ProductId { get; set; }

        [Column]
        public int ActId { get; set; }

        [Column]
        public int Amount { get; set; }

        [Association(ThisKey = nameof(ProductId), OtherKey = nameof(Entities.Product.Id))]
        public Product Product { get; set; }
    }
}
