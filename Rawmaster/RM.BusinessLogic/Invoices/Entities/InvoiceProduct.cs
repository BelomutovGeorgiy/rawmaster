﻿using LinqToDB.Mapping;
using RM.BusinessLogic.BusinessLogic.Core.DataAccess;

namespace RM.BusinessLogic.Invoices.Entities
{
    [Table("ProductInvoiceTransaction")]
    public class InvoiceProduct : IEntity
    {
        [SearchField("Id")]
        [Column("Id", IsIdentity = true)]
        [PrimaryKey]
        public int Id { get; set; }

        /// <summary>
        /// Id of the product.
        /// </summary>
        [Column]
        public int ProductId { get; set; }

        /// <summary>
        /// Id of the invoice.
        /// </summary>
        [Column]
        public int InvoiceId { get; set; }

        /// <summary>
        /// Price per tonn of product.
        /// </summary>
        [Column]
        public decimal PricePerTonn { get; set; }

        /// <summary>
        /// The VAT in %.
        /// </summary>
        [Column("VAT")]
        public int Vat { get; set; }

        /// <summary>
        /// Percent of products that toller left to the producer as a payment.
        /// </summary>
        [Column("NaturalPaymantPercent")]
        public int NaturalPaymentValue { get; set; }

        #region Navigation Fields

        [Association(ThisKey = nameof(ProductId), OtherKey = nameof(Entities.Product.Id))]
        public Product Product { get; set; }

        #endregion
    }
}
