﻿using System;
using System.Collections.Generic;
using LinqToDB;
using LinqToDB.Mapping;
using RM.BusinessLogic.BusinessLogic.Core.DataAccess;
using RM.BusinessLogic.ClientManager.Entities;

namespace RM.BusinessLogic.Invoices.Entities
{
    [Table("Invoices")]
    public class Invoice : EntityBase
    {
        [Column]
        public int ClientId { get; set; }

        [Column]
        public OperationType OperationType { get; set; }

        [Column]
        public int Number { get; set; }

        [Column("Date", DataType = DataType.Date)]
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// A id of the material that used in invoice
        /// </summary>
        [Column("RawMaterialId")]
        public int RawId { get; set; }

        [Column]
        public bool IsCompleted { get; set; }

        [Column(DataType = DataType.Date)]
        public DateTime ShipDate { get; set; }

        [Column]
        public decimal Aproximate { get; set; }

        /// <summary>
        /// A VAT amount in percents.
        /// </summary>
        [Column("VAT")]
        public int Vat { get; set; }

        /// <summary>
        /// A Price for processing of the one tonn of the raw material.
        /// </summary>
        [Column]
        public decimal PricePerTonn { get; set; }

        #region NavigationFields

        [Association(ThisKey = nameof(Id), OtherKey = nameof(InvoiceProduct.InvoiceId))]
        public List<InvoiceProduct> Products { get; set; }

        [Association(ThisKey = nameof(ClientId), OtherKey = nameof(RM.BusinessLogic.ClientManager.Entities.Client.Id))]
        public Client Client { get; set; }

        [Association(ThisKey = nameof(Id), OtherKey = nameof(Act.InvoiceId))]
        public List<Act> Acts { get; set; }

        [Association(ThisKey = nameof(RawId), OtherKey = nameof(Entities.RawMaterial.Id))]
        public RawMaterial RawMaterial { get; set; }

        #endregion
    }
}
