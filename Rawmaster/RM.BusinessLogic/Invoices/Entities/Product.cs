﻿using LinqToDB.Mapping;
using RM.BusinessLogic.BusinessLogic.Core.DataAccess;

namespace RM.BusinessLogic.Invoices.Entities
{
    [Table("Products")]
    public class Product : IEntity
    {
        [SearchField("Id")]
        [Column("Id", IsIdentity = true)]
        [PrimaryKey]
        public int Id { get; set; }

        [Column]
        public string Name { get; set; }

        [Column("Raw_Id")]
        public int RawId { get; set; }
    }
}
