﻿using System.Collections.Generic;
using LinqToDB.Mapping;
using RM.BusinessLogic.BusinessLogic.Core.DataAccess;

namespace RM.BusinessLogic.Invoices.Entities
{
    /// <summary>
    /// A raw material that toller give to the producer
    /// </summary>
    [Table("RawMaterials")]
    public class RawMaterial : IEntity
    {
        /// <summary>
        /// Id of the material
        /// </summary>
        [SearchField("Id")]
        [Column("Id", IsIdentity = true)]
        [PrimaryKey]
        public int Id { get; set; }
        
        /// <summary>
        /// Name of the material
        /// </summary>
        [Column]
        public string Name { get; set; }

        /// <summary>
        /// A List of products that can be made from this raw materials
        /// </summary>
        [Association(ThisKey = nameof(Id), OtherKey = nameof(Product.RawId))]
        private List<Product> Products { get; set; }
    }
}
