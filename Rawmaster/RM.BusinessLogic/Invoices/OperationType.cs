﻿namespace RM.BusinessLogic.Invoices
{
    public enum OperationType
    {
        Exchange,
        Contractual,
        Tolling
    }
}
