﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqToDB;
using RM.BusinessLogic.BusinessLogic.Core.DataAccess;
using RM.BusinessLogic.BusinessLogic.Core.Services;
using RM.BusinessLogic.Invoices.Entities;
using RM.BusinessLogic.Invoices.ReadModels;

namespace RM.BusinessLogic.Invoices.Services
{
    public interface IInvoiceService
    {
        List<Invoice> GetAll(bool withNestedItems, bool showArchived);

        List<Act> GetActsByInvoiceId(int invoiceId);

        List<InvoiceDetails> GetInvoiceDetails(int invoiceId);

        Invoice GetWithFullLoadById(int invoiceId);

        /// <summary>
        /// Get a list of products that can be made by current invoice
        /// </summary>
        /// <param name="invoiceId">An Id of invoice</param>
        /// <returns>List of available products</returns>
        List<Product> GetAvailableProducts(int invoiceId);

        void CreaateProducTransactionsForInvoice(int invoiceId);

        void UpdateProducTransactionsForInvoice(List<InvoiceProduct> products);

        void UpdateInvoice(Invoice invoice);
    }

    public class InvoiceService : ServiceBase<Invoice>, IInvoiceService
    {
        private readonly IRepository<Act> _actRepository;
        private readonly IRepository<Product> _productRepository;
        private readonly IRepository<InvoiceProduct> _invoiceProductRepository;

        public InvoiceService(IRepository<Invoice> repository,
            IRepository<Act> actRepository,
            IRepository<Product> productRepository,
            IRepository<InvoiceProduct> invoiceProductRepository) : base(repository)
        {
            _actRepository = actRepository;
            _productRepository = productRepository;
            _invoiceProductRepository = invoiceProductRepository;
        }

        public List<Act> GetActsByInvoiceId(int invoiceId)
        {
            return _actRepository.GetAll().Where(x => x.InvoiceId == invoiceId).ToList();
        }

        public List<Invoice> GetAll(bool withNestedItems, bool showArchived)
        {
            var items = ((ITable<Invoice>)Repository.GetAll()).LoadWith(x => x.Client);
            return items.Where(x => showArchived || x.Archived == 0).ToList();
        }

        public List<InvoiceDetails> GetInvoiceDetails(int invoiceId)
        {
            throw new System.NotImplementedException();
        }

        public Invoice GetWithFullLoadById(int invoiceId)
        {
            var invoice =
                ((ITable<Invoice>)Repository.GetAll()).LoadWith(x => x.Acts.First().Products.First().Product)
                .LoadWith(x => x.Products.FirstOrDefault().Product).LoadWith(x => x.Client)
                .FirstOrDefault(x => x.Id == invoiceId);
            return invoice;
        }

        /// <inheritdoc/>
        public List<Product> GetAvailableProducts(int invoiceId)
        {
            var invoice = GetById(invoiceId);
            var products = _productRepository.GetAll().Where(x => x.RawId == invoice.RawId);

            return products.ToList();

        }

        public void CreaateProducTransactionsForInvoice(int invoiceId)
        {
            var invoice = GetById(invoiceId);
            var products = _productRepository.GetAll().Where(x => x.RawId == invoice.RawId);
            var invoiceProducts = new List<InvoiceProduct>();
            foreach (var p in products)
            {
                invoiceProducts.Add(new InvoiceProduct
                {
                    InvoiceId = invoiceId,
                    ProductId = p.Id,
                    PricePerTonn = 0,
                    Vat = 0,
                    NaturalPaymentValue = 0
                });
            }
            _invoiceProductRepository.AddRange(invoiceProducts);
        }

        public void UpdateProducTransactionsForInvoice(List<InvoiceProduct> products)
        {
            foreach (var p in products)
            {
               // var product = _invoiceProductRepository.GetAll()
               //     .FirstOrDefault(x => x.ProductId == p.ProductId && x.InvoiceId == p.InvoiceId);
               //
               // product.Vat = p.Vat;
                _invoiceProductRepository.Update(p.Id, p);
            }
        }

        public void UpdateInvoice(Invoice invoice)
        {
            Repository.Update(invoice.Id, invoice);
        }
    }
}
