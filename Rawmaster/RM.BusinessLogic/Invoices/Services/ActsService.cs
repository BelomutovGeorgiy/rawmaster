﻿using System.Collections.Generic;
using System.Linq;
using RM.BusinessLogic.BusinessLogic.Core.DataAccess;
using RM.BusinessLogic.BusinessLogic.Core.Services;
using RM.BusinessLogic.Invoices.Entities;

namespace RM.BusinessLogic.Invoices.Services
{
    public interface IActsService
    {
        /// <summary>
        /// Add product transactions for act
        /// </summary>
        /// <param name="transactions">Id of the Act</param>
        void AddProductTransactions(List<ActProduct> transactions);

        /// <summary>
        /// Update Product amounts
        /// </summary>
        /// <param name="transactions">product list</param>
        void UpdateProductTransactions(List<ActProduct> transactions);
    }
    public class ActsService : ServiceBase<Act>, IActsService
    {
        private readonly IRepository<ActProduct> _actProductsRepository;

        public ActsService(IRepository<Act> repository,
            IRepository<ActProduct> actProductsRepository) : base(repository)
        {
            _actProductsRepository = actProductsRepository;
        }

        public void AddProductTransactions(List<ActProduct> transactions)
        {
            _actProductsRepository.AddRange(transactions);
        }

        public void UpdateProductTransactions(List<ActProduct> transactions)
        {
            // TODO: Remove terrible workaround 
            foreach (var t in transactions)
            {
                var transaction = _actProductsRepository.GetAll().FirstOrDefault(x => x.ActId == t.ActId &&
                                                                             x.ProductId == t.ProductId);

                transaction.Amount = t.Amount;

                _actProductsRepository.Update(transaction.Id, transaction);
            }
        }
    }
}
