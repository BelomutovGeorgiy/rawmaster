﻿using System;
using System.Collections.Generic;
using System.Windows;
using Caliburn.Micro;
using Ninject;

namespace RM.UI.Shared
{
    public class SharedBootstrapper : BootstrapperBase
    {
        private IKernel _kernel;

        public SharedBootstrapper()
        {
            Initialize();
        }

        protected override object GetInstance(Type service, string key)
        {
            return _kernel.Get(service);
        }

        protected override void Configure()
        {
            _kernel = new StandardKernel();
        }

        protected override void OnExit(object sender, EventArgs e)
        {
            _kernel.Dispose();
            base.OnExit(sender, e);
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return _kernel.GetAll(service);
        }

        protected override void BuildUp(object instance)
        {
            _kernel.Inject(instance);
        }
    }
}
