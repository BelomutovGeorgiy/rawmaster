﻿using System;

namespace RM.UI.Shared.Utils.SearchComponent
{
    public class SearchProperty
    {
        public string DisplyName { get; set; }

        public Type Type { get; set; }

        public string PropertyName { get; set; }
    }
}
