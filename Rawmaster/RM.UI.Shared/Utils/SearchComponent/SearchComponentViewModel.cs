﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Caliburn.Micro;
using Ninject.Infrastructure.Language;
using RM.BusinessLogic.BusinessLogic.Core.DataAccess;
using RM.BusinessLogic.BusinessLogic.Core.Services;

namespace RM.UI.Shared.Utils.SearchComponent
{
    public class SearchComponentViewModel<T> : Screen where T : EntityBase
    {
        private readonly  IServiceBase<T> _entityService;

        private SearchProperty _selectedProperty;

        public List<SearchProperty> Properties { get; set; }

        public SearchProperty SelectedProperty
        {
            get { return _selectedProperty; }
            set
            {
                Set(ref _selectedProperty, value);
                NotifyOfPropertyChange(nameof(_selectedProperty.Type));
            }
        }

        public object SearchValue { get; set; }

        public List<T> Results { get; set; }

        public SearchCriteria SearchCriteria { get; set; }

        public SearchComponentViewModel(IServiceBase<T> entityService)
        {
            _entityService = entityService;
            Properties = typeof(T).GetProperties()
                .Where(x => x.HasAttribute(typeof(SearchFieldAttribute)))
                .Select(x => new SearchProperty
                {
                    DisplyName = x.GetCustomAttribute<SearchFieldAttribute>().DisplayName,
                    PropertyName = x.Name,
                    Type = x.PropertyType
                })
                .ToList();
        }

        public List<T> Search(bool includeArchived)
        {
            Results = _entityService.GetByPredicate(BuildPredicate());
            return Results;
        }

        private Expression<Func<T, bool>> BuildPredicate()
        {
            var itemType = typeof(T);
            var predParam = Expression.Parameter(itemType, nameof(T));
            var property = itemType.GetProperty(SelectedProperty.PropertyName);
            var left = Expression.Property(predParam, property);
            var right = Expression.Constant(Convert.ChangeType(SearchValue, property.PropertyType), property.PropertyType);


            Expression equality = Expression.Equal(left, right);
            if (SearchCriteria != SearchCriteria.Equals)
            {
                MethodInfo methodInfo = null;
                switch (SearchCriteria)
                {
                    case SearchCriteria.Begins:
                        methodInfo = typeof(String).GetMethod(nameof(String.StartsWith));
                        break;
                    case SearchCriteria.Contains:
                        methodInfo = typeof(String).GetMethod(nameof(String.Contains));
                        break;
                    case SearchCriteria.Ends:
                        methodInfo = typeof(String).GetMethod(nameof(String.EndsWith));
                        break;
                }
                equality = Expression.Call(left, methodInfo, right);
            }
            

            var function = (Expression<Func<T, bool>>)Expression.Lambda(equality, new[] { predParam });
            return function;
        }

    }
}
