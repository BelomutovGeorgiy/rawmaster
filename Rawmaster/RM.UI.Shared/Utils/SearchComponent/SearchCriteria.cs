﻿namespace RM.UI.Shared.Utils.SearchComponent
{
    public enum SearchCriteria
    {
        Equals,
        Contains,
        Begins,
        Ends
    }
}
