﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace RM.UI.Shared.Utils.Converters
{
    public class EnumToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return parameter?.Equals(value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && (bool)value)
            {
                return parameter;
            }
            return DependencyProperty.UnsetValue;

        }
    }
}
