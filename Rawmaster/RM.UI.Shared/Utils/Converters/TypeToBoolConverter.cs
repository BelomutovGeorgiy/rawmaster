﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace RM.UI.Shared.Utils.Converters
{
    public class TypeToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return parameter.GetType() == typeof(string);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // NOT REQUIRED
            throw new NotImplementedException();
        }
    }
}
