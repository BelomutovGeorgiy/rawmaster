﻿using Caliburn.Micro;
using RM.BusinessLogic.BusinessLogic.Core.DataAccess;

namespace RM.UI.Shared.MasterDetail
{
    public class ItemViewModel<T> : Screen where T : EntityBase
    {
        private T _model;

        public T Model
        {
            get { return _model; }
            internal set
            {
                _model = value;
                Set(ref _model, value);
                NotifyOfPropertyChange(nameof(Model));
                NotifyOfPropertyChange(nameof(ItemName));
                OnModelChange();
            }
        }

        public virtual string ItemName => Model.Id + " - " + Model;

        public bool IsArchived
        {
            get { return Model.IsArchieved; }
        }

        public ItemViewModel(T model)
        {
            Model = model;
        }

        public virtual void OnModelChange()
        {

        }
    }
}
