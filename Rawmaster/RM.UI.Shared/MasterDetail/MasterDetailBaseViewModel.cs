﻿using System;
using System.Collections.ObjectModel;
using Ninject;
using RM.BusinessLogic.BusinessLogic.Core.DataAccess;
using RM.BusinessLogic.BusinessLogic.Core.Services;
using RM.UI.Shared.Utils.SearchComponent;

namespace RM.UI.Shared.MasterDetail
{
    public abstract class MasterDetailBaseViewModel<T> : ScreenBase where T : EntityBase
    {
        protected IServiceBase<T> EntityService;

        protected bool showArchieved;

        protected bool IsNewItem = true;

        private ItemViewModel<T> _detailItemViewModel;

        private T _selectedItem;

        private SearchComponentViewModel<T> _searchComponent;

        public SearchComponentViewModel<T> SearchComponent
        {
            get { return _searchComponent; }
            set { Set(ref _searchComponent, value); }
        }

        public ItemViewModel<T> DetailsViewModel
        {
            get { return _detailItemViewModel; }
            set { Set(ref _detailItemViewModel, value); }
        }

        public ObservableCollection<T> Items { get; set; }

        public T SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                Set(ref _selectedItem, value);
                if (value != null)
                {
                    IsNewItem = false;
                }

                if (_detailItemViewModel != null)
                {
                    _detailItemViewModel.Model = value;
                }
                OnSelectionChanged();
            }
        }

        public T OperativeItem { get; set; }

        public bool ShowArchieved
        {
            get { return showArchieved;}
            set
            {
                Set(ref showArchieved, value);
                RefreshItems();
            }
        }

        protected override void OnInitialize()
        {
            RefreshItems();

            SearchComponent = new SearchComponentViewModel<T>(EntityService);
            NotifyOfPropertyChange(nameof(DetailsViewModel));
            NotifyOfPropertyChange(nameof(DetailsViewModel));
        }

        [Inject]
        public void Inject(IServiceBase<T> entityService)
        {
            EntityService = entityService;
        }

        protected virtual void RefreshItems()
        {
            Items = new ObservableCollection<T>(EntityService.GetAll(showArchieved));
            NotifyOfPropertyChange(nameof(Items));
        }

        protected virtual void NotifyFields()
        {
            NotifyOfPropertyChange(nameof(CanArchiveCommand));
            NotifyOfPropertyChange(nameof(CanRestoreCommand));
            NotifyOfPropertyChange(nameof(CanSaveCommand));
        }

        protected virtual void Save()
        {
            if (IsNewItem)
            {
                var id = EntityService.Add(SelectedItem);
                SelectedItem.Id = id;
            }
            else
            {
                EntityService.Update(SelectedItem);
            }
            RefreshItems();
        }

        protected virtual void OnSelectionChanged()
        {
            NotifyFields();
        }

        public bool CanSaveCommand => SelectedItem != null;

        public void SaveCommand()
        {
            Save();
        }

        public void NewCommand()
        {
            SelectedItem = Activator.CreateInstance<T>();
            IsNewItem = true;
        }

        public void SelectCommand()
        {
            //if (SelectedItem.IsArchieved)
            //{
            //    WindowManager.
            //}

            // TODO: Add Validation
            TryClose(true);
        }

        public bool CanArchiveCommand => SelectedItem != null && !SelectedItem.IsArchieved;

        public void ArchiveCommand()
        {
            SelectedItem.IsArchieved = true;
            EntityService.Update(SelectedItem);
            RefreshItems();
        }

        public bool CanRestoreCommand => SelectedItem != null && SelectedItem.IsArchieved;

        public void RestoreCommand()
        {
            SelectedItem.IsArchieved = false;
            EntityService.Update(SelectedItem);
            RefreshItems();
        }

        public void SearchCommand()
        {
            var items = SearchComponent.Search(showArchieved);
            Items = new ObservableCollection<T>(items);
            NotifyOfPropertyChange(nameof(Items));
        }
    }
}
