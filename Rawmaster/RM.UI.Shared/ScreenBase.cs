﻿using System;
using Caliburn.Micro;
using Ninject;

namespace RM.UI.Shared
{
    public class ScreenBase : Screen
    {

        protected IWindowManager WindowManager;

        [Inject]
        public void Inject(IWindowManager windowManager)
        {
            WindowManager = windowManager;
        }

        protected void OpenWindow<T>(params object[] args)
        {
            var model = Activator.CreateInstance(typeof(T), args);
            IoC.BuildUp(model);
            WindowManager.ShowDialog(model);
        }
    }
}
